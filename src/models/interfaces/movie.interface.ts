export interface Movie {
  id: string;
  title: string;
  rating: number;
  picture: string;
  genres: string[];
}
