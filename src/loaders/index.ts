export { loadAllMovies, loadMoviesByGenre } from './loadMovies';
export { loadGenres } from './loadGenres';
