export { ReactComponent as StarIcon } from './Star.svg';
export { ReactComponent as LaunchIcon } from './Launch.svg';
export { ReactComponent as PlayIcon } from './Play.svg';
export { ReactComponent as GitHubIcon } from './Github.svg';
